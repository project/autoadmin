<?php

/**
 * @file
 * Handles the list page.
 *
 * @ingroup autoadmin
 * @author Rune Kaagaard rune@prescriba.com
 */

// Constants.
define('AUTOADMIN_LIST_LIMIT', 20);
define('AUTOADMIN_LIST_TRUNCATE', 60);

/**
 * Generates list view and returns themed html.
 *
 * @param array $schema
 *   Current schema.
 * @return string
 *   Html for output.
 */
function autoadmin_list($table) {
  $schema = autoadmin_init_page($table);
  $query = autoadmin_query_get_list_query($schema);
  $header = _autoadmin_list_header($schema);
  $query['sql'] .= tablesort_sql($header);
  $result = pager_query($query['sql'], AUTOADMIN_LIST_LIMIT, 0, NULL,
                        $query['placeholders']);
  $rows = _autoadmin_list_rows($result, $schema);
  return
    autoadmin_get_page_description($schema)
    . theme('table', $header, $rows)
    . theme('pager', NULL, AUTOADMIN_LIST_LIMIT)
  ;
}

/**
 * Returns header array for list view.
 *
 * @param array $schema
 * @return array
 */
function _autoadmin_list_header($schema) {
  $header = array();
  foreach ($schema['list_fields'] as $v) {
    if (!is_array($v)) {
      $header[] = array(
        'data' => $schema['fields'][$v]['title'],
        'field' => $v,
      );
    } else {
      if (!empty($v['order_field'])) {
        $header[] = array('data' => $v['title'], 'field' => $v['order_field']);
      } else {
        $header[] = $v['title'];
      }
    }
    
  }
  $header[] = t('Operations');
  return $header;
}

/**
 * Fetches rows from page query result.
 *
 * @param DbObject $result
 * @param array $schema
 * @return array
 */
function _autoadmin_list_rows($result, $schema) {
  $rows = array();
  while ($db_row = db_fetch_array($result)) {
    $rows[] = _autoadmin_list_row($db_row, $schema);
  }
  if (!$rows) {
    return _autoadmin_list_no_rows($schema);
  }
  return $rows;
}

/**
 * Returns a row with text displaying that no rows has been added.
 *
 * @param array $schema
 * @return array
 */
function _autoadmin_list_no_rows($schema) {
  $rows = array();
  // Fill td's to fit header th's.
  $rows[0] = array_fill(0, count($schema['list_fields']), '');
  array_unshift($rows[0], t('No records have been added.'));
  return $rows;
}

/**
 * Generate td's for a row.
 *
 * @param array $db_row a row from db query
 * @param array $schema
 * @return array a row to put in the list table
 */
function _autoadmin_list_row($db_row, $schema) {
  $table_row = array();
  foreach ($schema['list_fields'] as $v) {
    if (!is_array($v)) {
      $field_key = $v;
      $field = $schema['fields'][$v];
      if ($field['autoadmin_type'] != 'bool') {
        $value = check_plain(truncate_utf8($db_row[$v],
                             AUTOADMIN_LIST_TRUNCATE, FALSE, TRUE));
      } else {
        $value = _autoadmin_list_field_bool($db_row, $v, $schema);
      }
    } else {
      $field_key = $v['title'];
      $field = !empty($v['field']) && !empty($schema['fields'][$v['field']])
        ? $schema['fields'][$v['field']]
        : FALSE;
      $field_list_settings = $v;
      $value = $v['callback']($db_row, $field_list_settings, $field, $schema);
      if (empty($v['allow_html'])) {
        $value = check_plain($value);
      }
    }
    $table_row[$field_key] = $value;
  }
  // Add operation links.
  $table_row[] =
      _autoadmin_list_operations($db_row, $table_row, $schema)
    . _autoadmin_list_operations_has_many($schema, autoadmin_get_pv($db_row,
                                                                    $schema));
  return $table_row;
}

/**
 * Create edit and delete links.
 *
 * @param array $table_row
 * @param array $schema
 * @return string
 */
function _autoadmin_list_operations($db_row, $table_row, $schema) {
  $primary_value = $db_row[$schema['primary key'][0]];
  $operations = array(
    'edit' => array(
      'title' => t('Edit'),
      'href' => $schema['path'] . '/' . $primary_value . '/edit',
    ),
    'copy' => array(
      'title' => t('Copy'),
      'href' => $schema['path'] . '/' . $primary_value . '/copy',
    ),
    'delete' => array(
      'title' => t('Delete'),
      'href' => $schema['path'] . '/' . $primary_value . '/delete',
    ),
  );
  return theme('autoadmin_operations', $operations);
}

/**
 * Create links to subschemas where current schema has one_to_many relations.
 *
 * @param array $schema
 * @param int $primary_value
 * @return string html
 */
function _autoadmin_list_operations_has_many($schema, $primary_value) {
  if (empty($schema['relations_by_type']['has_many'])) {
    return NULL;
  }
  foreach ($schema['relations_by_type']['has_many'] as $relation) {
    $foreign_schema = autoadmin_schema_get_one($relation['foreign_table']);
    $links[] = array(
      'title' => t('Edit !title_plural', array('!title_plural' => $foreign_schema['title_plural'])),
      'href' => $foreign_schema['path'],
      'query' => array($relation['foreign_key'] => $primary_value),
    );
  }
  return theme('autoadmin_operations', $links);
}

/**
 * Creates list value for bool fields.
 * 
 * @param array $db_row
 * @param string $field_key
 * @param array $schema
 * @return string
 */
function _autoadmin_list_field_bool($db_row, $field_key, $schema) {
  $value = $db_row[$field_key];
  if (!empty($schema['fields'][$field_key]['#form']['#options'][$value])) {
    return $schema['fields'][$field_key]['#form']['#options'][$value];
  }
  else {
    $choices = array(t('No'), t('Yes'));
    return $choices[$value];
  }
}