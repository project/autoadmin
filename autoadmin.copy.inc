<?php

/**
 * @file
 * Handles the copy page.
 *
 * @ingroup autoadmin
 * @author Rune Kaagaard rune@prescriba.com
 */

/**
 * Build the copy confirmation form.
 *
 * @param array $form_state
 * @param array $schema
 * @param int $primary_value
 * @return array the return form
 */
function autoadmin_copy_confirm($form_state, $table, $primary_value) {
  $schema = autoadmin_init_page($table);
  $query = autoadmin_query_get_query($schema, $primary_value);
  $row = db_fetch_array(db_query($query['sql'], $query['placeholders']));
  $form['schema'] = array(
    '#type' => 'value',
    '#value' => $schema,
  );
  $form['primary_value'] = array(
    '#type' => 'value',
    '#value' => $primary_value,
  );
  autoadmin_theme_cascading_items($schema, $primary_value, $html);
  if ($html) {
    $form['cascading_html'] = array(
      '#value' => "<h3>The following items will be copied:</h3>
                  . <ul>$html</ul>",
    );
  }
  return confirm_form(
    $form,
    t('Are you sure you want to copy the %alias: %title?', array(
        '%alias' => $schema['title'], '%title' => $row[$schema['alias']])),
    isset($_GET['destination']) ? $_GET['destination'] : $schema['path'] . '/list',
    t('This action cannot be undone.'),
    t('Copy'),
    t('Cancel')
  );
}

/**
 * Copies rows.
 *
 * @param array $form
 * @param array $form_state
 */
function autoadmin_copy_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] === 1) {
    $pk_new = autoadmin_copy_recursively(
      $form_state['values']['schema'], 
      $form_state['values']['primary_value'],
      $i
    );
    drupal_set_message(t('Copied the %title from id %pk_old to id %pk_new. Also copied %i related items.', array(
      '%title'=> $form_state['values']['schema']['title'],
      '%pk_old' => $form_state['values']['primary_value'], 
      '%pk_new' => $pk_new,
      '%i' => $i,  
    )));
  }
  
  $form_state['redirect'] = $form_state['values']['schema']['path'] . '/list';
}

/**
 * Copy a database row for a schema and recursively copies all has_many 
 * relations for that row with updated relation id.
 * 
 * @param array $schema
 * @param int $primary_value
 * @param array $foreign_relation
 * @param int $new_related_value 
 */
function autoadmin_copy_recursively($schema, $primary_value, &$i=-1, 
$foreign_relation=false, $new_related_value=false) {
  $row = $row_new = autoadmin_db_fetch_row($schema, $primary_value);
  $pk_name = $schema['primary key'][0];
  unset($row_new[$pk_name]);
  if ($foreign_relation) {
    $row_new[$foreign_relation['foreign_key']] = $new_related_value;
  }
  drupal_write_record($schema['table'], $row_new);
  foreach ($schema['relations'] as $relation) {
    if ($relation['type'] == 'has_many') {
      $foreign_schema = autoadmin_schema_get_one($relation['foreign_table']);
      foreach (autoadmin_db_fetch_foreign_deletes($relation, $row) as $_row) {
        $_pv = autoadmin_get_pv($_row, $foreign_schema);
        autoadmin_copy_recursively($foreign_schema, $_pv, $i, $relation, 
                                   $row_new[$pk_name]);
      }
    }
  }
  ++$i;
  return autoadmin_get_pv($row_new, $schema);
}