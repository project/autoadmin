<?php

/**
 * @file
 * Handles the delete page.
 *
 * @ingroup autoadmin
 * @author Rune Kaagaard rune@prescriba.com
 */

/**
 * Build the delete confirmation form.
 *
 * @param array $form_state
 * @param array $schema
 * @param int $primary_value
 * @return array the return form
 */
function autoadmin_delete_confirm($form_state, $table, $primary_value) {
  $schema = autoadmin_init_page($table);
  $query = autoadmin_query_get_query($schema, $primary_value);
  $row = db_fetch_array(db_query($query['sql'], $query['placeholders']));
  $form['schema'] = array(
    '#type' => 'value',
    '#value' => $schema,
  );
  $form['primary_value'] = array(
    '#type' => 'value',
    '#value' => $primary_value,
  );
  $pks_by_table = array();
  $form['deletes'] = array(
    '#type' => 'value',
    '#value' => &$pks_by_table,
  );
  autoadmin_theme_cascading_items($schema, $primary_value, $html, 
                                  $pks_by_table);
  if ($html) {
    $form['cascading_html'] = array(
      '#value' => "<h3>The following items will be deleted:</h3>
                  . <ul>$html</ul>",
    );
  }
  return confirm_form(
    $form,
    t('Are you sure you want to delete the %alias: %title?', array('%alias' => $schema['title'], '%title' => $row[$schema['alias']])),
    isset($_GET['destination']) ? $_GET['destination'] : $schema['path'] . '/list',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Deletes rows.
 *
 * @param array $form
 * @param array $form_state
 */
function autoadmin_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] === 1) {
    foreach ($form_state['values']['deletes'] as $table => $keys) {
      $schema = autoadmin_schema_get_one($table);
      autoadmin_db_delete_where_pk_in($schema, $keys);
      $count = db_affected_rows();
      $title = $count < 2 ? $schema['title'] : $schema['title_plural'];
      drupal_set_message(t('Deleted %count %type.', array('%count' => $count, '%type' => $title)));
    }
  }
  $form_state['redirect'] = $form_state['values']['schema']['path'] . '/list';
}