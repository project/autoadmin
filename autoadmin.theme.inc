<?php

/**
 * @file
 * Theme functions.
 *
 * @ingroup autoadmin
 * @author Rune Kaagaard rune@prescriba.com
 */

/**
 * Implementation of hook_theme().
 */
function autoadmin_theme() {
  return array(
    'autoadmin_operations' => array('arguments' => array('text' => NULL, 'origin' => NULL)),
    'autoadmin_description' => array('arguments' => array('text' => NULL, 'origin' => NULL)),
  );
}

/**
 * Render a operation link. If more than one link add padding between them.
 *
 * @ingroup themeable
 * @param array $operations
 * @return string html
 */
function theme_autoadmin_operations($operations) {
  $html = '';
  $base_path = base_path();
  foreach ($operations as $operations => $operation) {
    $query = !empty($operation['query']) ? $operation['query'] : array();
    $operation['title'] = str_replace(' ', '&nbsp;', $operation['title']);
    $html .= l(
      $operation['title'],
      $operation['href'],
      array(
        'query' => $query,
        'attributes' => array('class' => 'operations'),
        'html' => TRUE,
      )
    );
  }
  return $html;
}

/**
 * Renders description of a schema.
 *
 * @ingroup themeable
 * @param <type> $schema
 * @return <type>
 */
function theme_autoadmin_description($schema) {
  return $schema['title_plural'] . 
    (!empty($schema['description'])
      ? ' <span class="autoadmin_description">(' . trim($schema['description'], '.') . ')</span>'
      : ''
    );
}

/**
 * Display items to be copied cascadingly and add to copys. This function is
 * not using the drupal theme system yet.
 * 
 * @param type $schema
 *   The schema for which the item to be copied belongs.
 * @param type $primary_value
 *   Primary key of item to be copied.
 * @param type $copys
 *   Set by reference. Items to be copied by table an pk.
 * @return string
 *   The warning html. 
 */
function autoadmin_theme_cascading_items($schema, $primary_value, &$html='', 
&$pks_by_table=array()) {
  $row = $row_new = autoadmin_db_fetch_row($schema, $primary_value);
  if (empty($row)) {
    return;
  }
  $pk_name = $schema['primary key'][0];
  $pv = autoadmin_get_pv($row, $schema);
  $href = url(base_path() . $schema['path'] . '/' . $pv . '/edit');
  $alias = $row[$schema['alias']];
  $html .= "
    <li>
      <strong>$schema[title]:</strong> 
      <a href='$href' target='_blank'>$pv - $alias</a>
  ";
  $pks_by_table[$schema['table']][] = $primary_value;
  foreach ($schema['relations'] as $relation) {
    if ($relation['type'] == 'has_many') {
      $foreign_schema = autoadmin_schema_get_one($relation['foreign_table']);
      $rows = autoadmin_db_fetch_foreign_deletes($relation, $row);
      if (!empty($rows)) {
        $html .= '<ul>';
        foreach ($rows as $_row) {
          $_pv = autoadmin_get_pv($_row, $foreign_schema);
          autoadmin_theme_cascading_items($foreign_schema, $_pv, $html, 
                                          $pks_by_table);
        }
        $html .= '</ul>';
      }
    }
  }
  $html .= '</li>';
}